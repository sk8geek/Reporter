/* 
 * @(#) Report.java    0.1 2012/02/18
 * 
 * An abstract class for reports about things, eg a potholes, stray dogs.
 * Copyright 2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.reporter;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
//import java.util.HashMap;

abstract public class Report extends Activity {
    
    protected String reportType = "";
    protected double locationLat = 0;
    protected double locationLon = 0;
    /** Standard Button for all types; show a map to get a location. */
    protected Button locate;
    /** Standard Button for all types; use camera to get a photo. */
    protected Button takePhoto;
    /** Standard Button for all types; store/submit the report. */
    protected Button submit;
    protected Camera camera;
    protected Camera.Parameters cameraParams;
    /** Email address to which the report should be sent. */
    protected String mailToAddress;
    protected MenuInflater inflater;
    protected static final int LOCATION_REQUEST = 1;
    protected static final int PHOTO_REQUEST = 2;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        camera = Camera.open();
        if (camera != null) {
            cameraParams = camera.getParameters();
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        inflater = getMenuInflater();
        inflater.inflate(R.menu.report, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.selectLocation:
            return locate.performClick();
        case R.id.takePhoto:
            return takePhoto.performClick();
        default:
            setContentView(R.layout.error);
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (camera != null) {
            camera.release();
        }
    }
    
    protected void inflateStandardButtons() {
        locate = (Button)findViewById(R.id.locateButton);
        takePhoto = (Button)findViewById(R.id.photoButton);
        submit = (Button)findViewById(R.id.submitButton);
        submit.setEnabled(false);
    }

//    public void setAttributes(String[] attribs) {
//        for (int i = 0; i < attribs.length; i++) {
//            attributes.put(attribs[i],"");
//        }
//    }
        
    public void getLocationFromMap(View v) {
        Intent i;
        i = new Intent(this, Map.class);
        startActivityForResult(i, LOCATION_REQUEST);
    }
    
//    public void getTypedLocation(View v) {
//        // TODO: give the user a method to enter location details manually
//    }
    
    public void takePhoto(View v) {
        Intent i;
        i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PHOTO_REQUEST);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        Log.d ("channele", "Report, onActivityResult:" + requestCode + ", " + resultCode);
        switch (requestCode) {
        case LOCATION_REQUEST :
            if (resultCode == RESULT_OK) {
                if (i.hasExtra("latitude") && i.hasExtra("longitude")) {
                    locationLat = i.getDoubleExtra("latitude", 0);
                    locationLon = i.getDoubleExtra("longitude", 0);
                    locate.setText(R.string.locationSet);
                    locate.setEnabled(false);
                    locate.invalidate();
                    submit.setEnabled(true);
                    submit.invalidate();
                } else {
                    Log.d ("channele", "Report, onActivityResult: extras missing!");
                }
            } else {
                //TODO: FIXME:
                //failed to get location from map, so do something, like 
                // ask the user to type an address or something
            }
        case PHOTO_REQUEST :
            // find the photo and do something with it
            if (resultCode == RESULT_OK) {
                Log.d ("channele", "result ok");
            }
        default :
            // do something
        }
    }

    abstract public void submitReport(View v);
    
    abstract public String getReport();
    
    public void setMailToAddress(String email) {
        mailToAddress = email;
    }
    
//    public void setGeo(boolean geo) {
//        geographical = geo;
//        if (geo) {
//            attributes.put("Latitude", "");
//            attributes.put("Longitude", "");
//        } else {
//            // TODO: remove these attributes if not geo
//        }
//    }
}
    
    
