/* 
 * @(#) CheapLocator.java    0.1 2012/02/21
 * 
 * A service that aims to provide a "cheap" location.
 * The service starts by checking passive location to see if it's 
 * probably usable. If it isn't then network and gps will be used.
 *
 * Copyright 2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.reporter;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

public class CheapLocator extends IntentService {
    
    /** Default value for timeliness of location data to be considered valid. */
    protected static int TEN_MINUTES = 10 * 60 * 60 *1000;
    /** Default accuracy for location data to be considered valid. */
    protected static int DEFAULT_ACCURACY_LIMIT = 100;
    /** Time limit for location data to be considered valid. */
    public int timeLimit = TEN_MINUTES;
    /** Accuracy limit for location data to be considered valid. */
    public int accuracyLimit = DEFAULT_ACCURACY_LIMIT;
    protected Criteria criteria = new Criteria();
    protected LocationListener listener;
    protected LocationManager manager;
    protected LocationProvider provider;
    protected Location loc;
    protected String bestProvider;
    
    public CheapLocator() {
        super("CheapLocator");
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d ("channele", "CheapLocator, onCreate()");
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(false);
        criteria.setSpeedRequired(false);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        manager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        listener = new LocationListener() {
            public void onLocationChanged(Location newLocation) {
                loc = newLocation;
            }
            
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
            
            public void onProviderEnabled(String provider) {
            }
            
            public void onProviderDisabled(String provider) {
            }
        };
        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
    }
    
    //@Override
    public void onDestroy() {
        //super.onDestroy();
        Log.d ("channele", "CheapLocator, onDestroy()");
        manager.removeUpdates(listener);
        manager = null;
        listener = null;
        provider = null;
        loc = null;
    }
    
    public void onHandleIntent(Intent i) {
        Log.d ("channele", "CheapLocator, onHandleIntent()");
        if (i.hasExtra("timeLimit")) {
            timeLimit = i.getIntExtra("timeLimit", timeLimit);
        }
        if (i.hasExtra("accuracyLimit")) {
            accuracyLimit = i.getIntExtra("accuracyLimit", accuracyLimit);
        }
        loc = manager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        Log.d ("channele", "CheapLocator:passive");
        if (!locationGoodEnough()) {
            bestProvider = manager.getBestProvider(criteria, true);
            Log.d ("channele", "CheapLocator:bestProvider=" + bestProvider);
            loc = manager.getLastKnownLocation(bestProvider);
            if (!locationGoodEnough()) {
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
                bestProvider = manager.getBestProvider(criteria, true);
                Log.d ("channele", "CheapLocator:bestProvider=" + bestProvider);
                loc = manager.getLastKnownLocation(bestProvider);
                if (!locationGoodEnough()) {
                    criteria.setPowerRequirement(Criteria.POWER_HIGH);
                    bestProvider = manager.getBestProvider(criteria, true);
                    Log.d ("channele", "CheapLocator:bestProvider=" + bestProvider);
                    while (!locationGoodEnough()) {
                        loc = manager.getLastKnownLocation(bestProvider);
                    }
                }
            }
        }
    }
    
    /**
     * Compare the current location to the limit values and if it's within
     * those ranges stop working.
     * @return false if location not considered valid
     */
    private boolean locationGoodEnough() {
        boolean goodEnough = false;
        if (loc != null) {
            if (System.currentTimeMillis() - loc.getTime() <= timeLimit) {
                if (loc.getAccuracy() < accuracyLimit) {
                    Log.d ("channele", "location:" + loc.getAccuracy());
                    goodEnough = true;
                }
            }
        }
        return goodEnough;
    }
}
