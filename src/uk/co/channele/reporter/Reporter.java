/* 
 * @(#) Reporter.java    0.1 2012/02/03
 * 
 * A report about something, eg a pothole or abandoned vehicle.
 * Copyright 2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.reporter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Reporter extends Activity implements OnClickListener {
    
    private Button pothole;
    private Button dogmess;
    private Button vehicle;
    private Button tipping;
    private Button obstruction;
    private MenuInflater inflater;
    private Intent locator;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        locator = new Intent(this, CheapLocator.class);
        startService(locator);
        pothole = (Button)findViewById(R.id.potholeButton);
        pothole.setOnClickListener(this);
        dogmess = (Button)findViewById(R.id.dogmessButton);
        dogmess.setOnClickListener(this);
        vehicle = (Button)findViewById(R.id.vehicleButton);
        vehicle.setOnClickListener(this);
        tipping = (Button)findViewById(R.id.tippingButton);
        tipping.setOnClickListener(this);
        obstruction = (Button)findViewById(R.id.obstructionButton);
        obstruction.setOnClickListener(this);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.setRecipients:
            setRecipients();
            return true;
        default:
            setContentView(R.layout.error);
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void setRecipients() {
    }
    
    /** Handle buttons */
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
        case R.id.potholeButton:
            i = new Intent(this, Pothole.class);
            startActivity(i);
        case R.id.dogmessButton:
            // i = new Intent(this, Dogmess.class);
        case R.id.vehicleButton:
            // i = new Intent(this, Vehicle.class);
        case R.id.tippingButton:
            // i = new Intent(this, Tipping.class);
        case R.id.obstructionButton:
            // i = new Intent(this, Obstruction.class);
        default:
            /* FIXME: trap this */
        }
    }
}
