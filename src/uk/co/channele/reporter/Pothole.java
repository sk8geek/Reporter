/* 
 * @(#) Pothole.java    0.1 2012/02/03
 * 
 * A report about something, eg a pothole or abandoned vehicle.
 * Copyright 2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.reporter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox; 
import android.widget.Toast;

public class Pothole extends Report implements OnClickListener {
    
    private CheckBox covered;
    private CheckBox waterFilled;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportType = "Pothole";
        setContentView(R.layout.pothole);
        covered = (CheckBox)findViewById(R.id.potholeCovered);
        covered.setOnClickListener(this);
        waterFilled = (CheckBox)findViewById(R.id.potholeWaterFilled);
        waterFilled.setOnClickListener(this);
        inflateStandardButtons();
    }
    
    public void onPause() {
        super.onPause();
    }
    
    public void submitReport(View v) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("rfc/822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.potholeEmail)});
        i.putExtra(Intent.EXTRA_SUBJECT, reportType + " Report");
        i.putExtra(Intent.EXTRA_TEXT, getReport());
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
    
    public String getReport() {
        StringBuilder report = new StringBuilder();
        report.append(getString(R.string.pothole));
        report.append(" Report\n" + getString(R.string.potholeCovered) + ": ");
        if (covered.isChecked()) {
            report.append(getString(R.string.yes));
        } else {
            report.append(getString(R.string.no));
        }
        report.append("\n" + getString(R.string.potholeWaterFilled) + ": ");
        if (waterFilled.isChecked()) {
            report.append(getString(R.string.yes));
        } else {
            report.append(getString(R.string.no));
        }
        report.append("\nLatitude:" + locationLat);
        report.append("\nLongitude:" + locationLon);
        return report.toString();
    }
    
    /** Handle buttons */
    public void onClick(View v) {
        switch (v.getId()) {
        default:
            // FIXME: handle this
        }
    }
}
