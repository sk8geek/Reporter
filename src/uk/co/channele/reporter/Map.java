/* 
 * @(#) Map.java    0.1 2012/02/18
 * 
 * Presents a map to allow the user to locate the issue to be reported.
 * Copyright 2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.reporter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class Map extends MapActivity {
    protected int halfMarkerSize;
    protected Button acceptLocation;
    protected Drawable marker;
    protected GeoPoint startingPoint;
    protected GeoPoint tapPoint;
    protected MapController mapCon;
    protected MapView mv;
    protected Location roughLocation;
    protected LocationManager locationManager;
    protected Overlay tapOverlay;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        mv = (MapView)findViewById(R.id.mapview);
        mv.setBuiltInZoomControls(true);
        mv.setSatellite(false);
        mv.setStreetView(false);
        mv.setTraffic(false);
        marker = mv.getResources().getDrawable(R.drawable.marker);
        halfMarkerSize = marker.getIntrinsicWidth() / 2;
        marker.setBounds(-halfMarkerSize, -halfMarkerSize, halfMarkerSize, halfMarkerSize);
        acceptLocation = (Button)findViewById(R.id.acceptLocationButton);
        mapCon = mv.getController();
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        roughLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (roughLocation != null) {
            startingPoint = new GeoPoint(new Double(roughLocation.getLatitude() * 1E6).intValue(),
                new Double(roughLocation.getLongitude() * 1E6).intValue());
            mapCon.setZoom(18);
            mapCon.animateTo(startingPoint);
        }
        tapOverlay = new TapOverlay();
        mv.getOverlays().add(tapOverlay);
    }
    
    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }
    
    @Override
    public void finish() {
        Intent imHere = new Intent();
        imHere.putExtra("latitude", tapPoint.getLatitudeE6() / 1E6);
        imHere.putExtra("longitude", tapPoint.getLongitudeE6() / 1E6);
        setResult(RESULT_OK, imHere);
        super.finish();
    }
    
    public void acceptLocation(View v) {
        finish();
    }
    
    protected class TapOverlay extends Overlay {
        private Point mapPoint;
        
        TapOverlay() {
        }
        
        @Override
        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            super.draw(canvas, mapView, shadow); //FIXME: does this fix the marker vanish issue?
            if (tapPoint != null) {
                mapPoint = mapView.getProjection().toPixels(tapPoint, mapPoint);
                drawAt(canvas, marker, mapPoint.x, mapPoint.y, false);
            }
        }
        
        @Override
        public boolean onTap(GeoPoint point, MapView mapView) {
            tapPoint = point;
            acceptLocation.setText(R.string.acceptLocation);
            acceptLocation.setEnabled(true);
            acceptLocation.invalidate();
            return true;
        }
    }
}

